---
layout: markdown_page
title: "Solutions Architects"
---

# Solutions Architect Handbook
{:.no_toc}

Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsiblities
See the [Solutions Architect job description](/roles/sales/solutions-architect/)

## When and How to Engage a Solutions Architect
Engaging GitLab Solutions Architects


### Before getting SA involved:

#### 1) Qualification:
* What is the qualified reason to engage with us?
* What is the size/quality/state of the opportunity?
* Have you created your plan?

#### 2) Preparation:

Help us, help them.  3 ASKs:
* Outcome.
* Infrastructure.
* Challenges.

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges (see below) information uncovered by the AM in prior interactions with the account.  Occasionally, we could support a combination call of discovery and technical demonstration/deep-dive, but this is suboptimal.  However, the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

**Outcome**
WIIFM/T.  What’s in it for them?  Why are they looking at a new strategy for s/w dev.  We need to be able to tailor our demos and presentations to demonstrate value and how we can address their issues.

**Infrastructure**
What i2p tools do they currently have in place?  E.g. Jenkins for CI, Ansible/Chef for automation, Codebear for review, VS/eclipse/emacs/vim for dev, etc.

**Challenges**
What problems/roadblocks are they having?  Release velocity, visibility, collaboration, automation.


## Engagement Protocol:

First, Navigate to the SA Service Desk Board [here](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477)

> NOTE: The board is located in the Group “Customer Success” and the name of the project is “SA Service Desk”

1) Create a [new issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)  
2) Use one of two templates...

*  If its the first engagement use the "SA Activity" template to ensure the proper data is collected about your needs.
*  If this is a follow up meeting, use the "Follow Up" template.

3) Add the “SA Backlog” label to the Issue  
4) The SA Group will triage the SA Backlog queue and collaborate with the submitter via Issue Discussions.

> NOTE: This will not replace SFDC.  The SAs will still be required to input the necessary information for each account that is critical to the strategy

* [Video: How to engage Solutions Architects using GitLab](https://drive.google.com/file/d/0BztS4JxtaDlrTnZIcWFhb0dWZ2c/view?ts=59879f01) Internal Only

## Triage Process

### Engaging with Solutions Architects
For engaging Solutions, Architects, see [this page on the Solutions Architects handbook](/handbook/customer-success/engaging-solutions-architect/).

### The SA Service Desk
The SA Group works from the [SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477).  This is the central place for the Sales Function to interact with the SA group and request assistance for customers.  Discovery Calls, POCs, Professional Service Engagements all start as an issue on the SA Service Desk.

To open an issue, visit the [new SA Service Desk issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and select the `SA Activity` template.  This template collects the basic information about the need for SA engagement.  

**Information to Collect**
To help the Solutions Architect group ensure success for your customer, we request the following information *prior* to SA engagement:

* **Customer Information**: Customer name, opportunity size, Sales Force link
* **Preferred Date Options**: When would the customer want to have the call (including time zone information).
* **Opportunity Details**: What is the size of the opportunity?  What is the timeframe for purchase?
* **Outcome**: What's in it for them?  
* **Challenges**: What roadblocks are they facing?  
* **Infrastructure**: What tools does the customer currently use?
* **SA Tasks**: What do you need from the SA's (demo/technical shotgun/services evaluation/etc.)

**Labels**
* **SA Backlog**: New issues coming into the SA
* **SA Triaged**: This label will change names to show which SA is triaging this week.  Once the activity has been vetted by the SA group, approved, and SA Ownership is assigned, it is moved to this label
* **SA Doing**: Once an SA signals they are actively working the Issue, the issue is moved to SA Doing.  The issue will stay in this status until the SA and AE agree the Issue has been completed
* **SA Action Items**: These are long-running strategic issue that the SA group is owning
* **SA Waiting**: This label indicates that the SA group is waiting on the AE for more information before advancing this issue.
* **EXPEDITE**: This label is only to be used if there is a turnaround required prior to the next Triage call
* **Current Quarter Label**:
* **Services**: If this opportunity involves or requires services, use this label to indicate that.

### Weekly SA Triage Assignment
Each week one specific Solutions Architect is assigned as the Triage SA for the week.  This is indicated both in the `SA Triaged` label as well as in the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

### Triage Call
The Solutions Architect group hosts a triage call three times a week.

The current week assigned Triage SA facilitates this call.  You can find who the current triager is on the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

The call is on Monday, Tuesday, and Thursday at 12:30 pm Easter,n 9:30 am Pacific.  We invited the entire company to join us on these calls, and you can join with [this Zoom link](https://gitlab.zoom.us/j/918526668)
