---
title: "Enhancing Organization with GitLab"
author: Katherine Okpara
author_gitlab: katokpara
author_twitter: katokpara
categories: inside GitLab
image_title: '/images/default-blog-image.png'
description: "Katherine Okpara discusses how she handled the learning process during her first month at GitLab, including how to stay organized and avoid getting overwhelmed."
---

Last December, I joined GitLab, Inc. as a Junior User Experience Researcher. 
My first month was awesome, rewarding, and sometimes challenging. 
Between meeting new teammates, diving deeper into my role, and familiarizing myself with our [handbook](https://about.gitlab.com/handbook/), [features](https://about.gitlab.com/features/), and product [direction](https://about.gitlab.com/direction/), I often felt like I was surrounded by an avalanche of information. 
Since I knew that organization would be the key to staying afloat, I decided to start using GitLab’s [planning](https://about.gitlab.com/features/#plan) tools as a way to stay focused while learning more about our product.

<!-- more --> 

## Optimizing my workflow
Before working at GitLab, I used Trello as my primary planning tool for anything from maintaining to-do lists, mapping out goals, and creating roadmaps for personal projects. Some of my favorite aspects of Trello included the ability to drag-and-drop items on boards and create a variety of labels. I soon learned that I could use all of these features and more through GitLab, instead of switching back and forth.

I transitioned from managing my daily work through Trello to setting up new processes with [GitLab Issues](https://about.gitlab.com/features/issueboard/). First, I created a personal project on GitLab.com and customized my Issues Board. Then, I use the board to prioritize the growing backlog of content I’d been curating since my first day on the job. I personally like to use Issues to keep track of readings and consolidate my notes in one easy-to-access place. Lastly, I created an Issue that serves as a continuous “To-Do” list for all the tasks I aim to accomplish each day. If any items are left unaddressed by the end of the day, I move them to the top of the list for the next day.

## A system for any need
After some trial and error, I settled on using this system to improve my daily productivity. Now equipped with an efficient system, I feel more prepared and eager to fully immerse in my journey at GitLab. 

If you’re looking for ways to boost your productivity, I’d highly recommend using Issues and [Epics](https://docs.gitlab.com/ee/user/group/epics/) to categorize your work by subject matter, level of priority, or any other factors that are important to you.

What tools help you stay organized?
