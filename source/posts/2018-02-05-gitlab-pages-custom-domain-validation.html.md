---
title: "GitLab Pages Security Issue Notification"
date: 2018-02-05
author: James Ritchey
author_gitlab: jritchey
categories: security
tags: security, gitlab
---

## Issue Summary

When a user adds a [custom domain] to their Pages site, no validation was being performed to ensure the domain was owned by that user. This issue allows an attacker to discover DNS records already pointing to the GitLab Page IP address which haven't been claimed and potentially hijack them. This issue impacts all users who have created and then deleted custom domains using GitLab Pages, but still have the DNS records active.

## Customer Remediation Steps

Our customers should check if they are using the GitLab Pages service with a custom domain and review their DNS records which point to the GitLab Pages IP `52.167.214.135`.

If you notice any of your DNS records pointing to that IP address and you're no longer using or intending to use the Gitlab Pages service, please remove those specific DNS records.

If you are intending to use the GitLab Pages service and notice that your custom domain has already been claimed or "hijacked", please contact us at security@gitlab.com.

## GitLab Remediation Strategy

We've currently disabled the feature to add custom domains until we've deployed the patch. In the meantime, the GitLab team is working to provide a more complete [validation] of custom domains in the GitLab Pages service as soon as possible.

Our mitigation strategy will consist of implementing domain verification mechanisms on all new and existing GitLab Pages domains, utilizing checks on customer DNS TXT records. This mechanism will be detailed in GitLab Pages documentation when implemented.

There will be a transition plan for current customers once the domain verification mechanisms are active. Stay tuned for further details.

#### Update (2018-02-07 10:00PM UTC)

The [issue] referenced above is currently confidential and will be made public after the fix is implemented in 10.5.


[custom domain]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html#adding-your-custom-domain-to-gitlab-pages
[validation]: https://gitlab.com/gitlab-org/gitlab-ce/issues/29497
[issue]: https://gitlab.com/gitlab-org/gitlab-ce/issues/29497
