---
layout: job_page
title: "Regional Sales Director"
---

As the Regional Sales Director, you will lead a dynamic sales team and help GitLab surpass our growth goals. The right candidate is responsible for building and leading a world class sales team, implementing technology and processes, and partnering with the account management, customer success, marketing, product, engineering and operations teams to deliver the best possible customer experience. The right candidate is truly passionate about open source software and has a proven track record to talk about! This is an exciting opportunity to unequivocally influence GitLab’s overall success and growth.

## Responsibilities

* Drives, manages and executes the business and revenue of a sales team; develops
* Analyzes regional market dynamics in an effort to maximize existing successes and to create new sales growth opportunities
* Educates team on significant industry factors including competitive products, regulations, trends, customer needs, and pricing
* Prepares forecasts, territory/industry management, and growth plans
* Establishes and reports on metrics to measure team performance; correct deficiencies where necessary
* Ensures that the regional sales plan is aligned with and supports the corporate revenue goal
* Recruits, hires and trains staff members; fosters a successful and positive team environment

## Requirements

* Process, data driven sales operations background. Strong experience with Salesforce.com, and building track-able, repeatable sales processes.
* Experience in a DevOps tools sales environment
* Willingness to “roll up the sleeves” and sell
* Experience selling to Fortune 500
* Ability to exercise effective judgment, sensitivity, creativity to changing needs and situations
* Must be adaptable, professional, courteous, motivated and work well on their own or as a member of a team
* Ability to handle a fast-paced environment and challenging workload
* Proven track record of meeting or exceeding performance objectives (revenue targets, pipeline targets, etc.)
* Experience utilizing CRM systems and marketing automation systems (such as salesforce, market, outreach etc.)
* Prior employment in technology consulting, open source software or software development is highly desired
* Demonstrate high levels of integrity, initiative, honesty and leadership

## Hiring Process

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
