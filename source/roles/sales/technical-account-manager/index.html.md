---
layout: job_page
title: "Technical Account Manager"
---

Are you passionate about customer success?  Do you have a proven customer acumen with a solid technical foundation? Then come be a member of GitLab's Customer Success Team. Providing guidance, planning and oversight while leveraging adoption and technical best practices. The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab.  Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

To learn more, see the [Technical Account Manager handbook](/handbook/customer-success/tam)

## Responsibilities

- Provide immediate on-boarding activities such as installation and training following investment of GitLab
- Own overall relationship with assigned clients, which include: increasing adoption, ensuring retention, and satisfaction
- Work with clients to build Customer Success Plans, establishing critical goals, or other key performance indicators and aid the customer in achieving their goals
- Measure and monitor customers achievement of critical and key performance indicators, reporting both internally to GitLab account stakeholders and externally to Customer Sponsors and Executives
- Establish regular cadence (weekly, Monthly, Quarterly) with each assigned clients, to review health metrics
- Establish a trusted/strategic advisor relationship with each assigned client and drive continued value of our solution and services
- Work closely with the GitLab Sales Account team (Account Executive, Solutions Architects, Professional Services) to identify opportunities for new usage of GitLab across organizational functions
- Work to identify and/or develop up-sell opportunities
- Advocate customer needs/issues cross-departmentally
- Program manage account escalations
- Assist and provide expert deployment, operational best practices and establishing a GitLab Center of Excellence
- Assist in workshops to help customers leverage the full value of GitLab solution
- Provide insights with respect to the availability and applicability of new features in GitLab
- Support GitLab Services in identifying and recommending training opportunities
- Act as the GitLab liaison for GitLab technical questions, issues or escalations.  This will include working with GitLab Support, Product Management(i.e. roadmaps), or others needed
- Maintain current functional knowledge and technical knowledge of GitLab platform

## Requirements

- 7 + years of experience in a related function is required with direct customer advocacy and engagement experience in post-sales or professional services functions
- Prior experience in Customer Success or equivalent history of increasing satisfaction, adoption, and retention
- Familiarity working with clients of all sizes, especially large enterprise organizations
- Exception verbal, written, organizational, presentation, and communications skills
- Detailed oriented and analytical
- Strong team player but self starter
- Strong technical, analytic and problem solving skills
- Experience with Ruby on Rails applications and Git
- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, chatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
- Installation and operation of Linux operating systems and hardware investigation/manipulation commands
- BASH/Shell scripting including systems and init.d startup scripts
- Package management (RPM, etc. to add/remove/list packages)
- Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

- Qualified applicants receive a short questionnaire.
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call).
- Candidates will be invited to schedule interviews with Director of Customer Success.
- Next, candidates will be invited to schedule an interview with members of the -Customer Success Team.
- Candidates may be invited to schedule an interview with our CRO.
- Finally, candidates may interview with our CEO.
- Successful candidates will subsequently be made an offer via email.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
