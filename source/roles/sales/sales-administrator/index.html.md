---
layout: job_page
title: "Sales Administrator"
---

As the Sales Administrator, you will play a critical role in support of our sales team.

You will report to the Director of Sales Operations and will support our sales team in a number of essential sales-related tasks such as generating quotes, sending legal documents, and working with Finance to complete vendor forms. You will also serve in a customer-facing capacity where you will answer questions related to a customer’s account, including questions related to renewing or upgrading their GitLab service. Finally, you will play an important role in the data quality of our systems, using third-party tools to enrich and keep our business systems up-to-date.

About you: You are comfortable multi-tasking while simultaneously paying very close attention to detail. You will also be highly organized and an excellent communicator. You like to follow process, almost to a fault.

If you are these things, and are passionate about sales support and can demonstrate how you’ve made the sales teams you’ve supported successful, we’d love to hear from you.

## Responsibilities

* Be the first point of contact to respond to/redirect the majority of non-technical inquiries via email
* Provide basic product information and more detailed information on the various subscriptions and services we offer
* Provide quotes, update licenses and account information or pass off the inquiry to Account Managers or Support Engineers
* Process sales orders, issue license keys, and manage a small sales pipeline of your own
* Work with our Controller on customer payments and other back-end finance related activities
* Support our Account Executives/Managers and Sales Development Representatives by assisting with the generation of proposals, and completing vendor forms and other documentation required by the prospect/customer.
* Follow up on prospect and customer inquiries sent to sales@ and contact@ and route to appropriate team.
* Support our SMB customers/prospects with purchasing and license issuance inquiries, account maintenance, renewals and upsells.
* Maintain data integrity within customer records in Salesforce.com and other systems. Serve as the primary administrator of our Salesforce instance and complete the usual tasks of which include field creation, layout modifications, and implementation of workflows and validation rules.
* Deliver basic reporting and analysis of KPIs in Salesforce and InsightSquared: bookings, pipeline/forecasting, churn, and other metrics
* Work with Sales Managers to increase forecast accuracy and integrity of pipeline
* Monitor system adoption and data compliance and governance
* Collaborate with other departments to improve integration between Salesforce and other mission-critical systems, including marketing automation, billing, engineering, and support
* Enrich our CRM with data from third-party sources.
* Contribute to documenting improvements in our [sales handbook](https://about.gitlab.com/handbook/sales/).
* Be the voice of the customer by contributing product ideas to our public [issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues).

## Requirements
* 2+ years of experience with B2B sales, preferably in a sales support/admin role. You should have a very solid understanding of the subscription business model.
* Interest in GitLab and open source software.
* Motivated, driven, and results oriented.
* Highly organized, with the ability to handle multiple tasks/deliverables at once.
* You’re familiar enough with a spreadsheet application (Googlesheets preferably) enough to create a pivot table and nice looking charts. If you love VLOOKUPS, even better.
* Experience with Salesforce.com. You know how to create workflows and use Process Builder. Maybe you’ve done some APEX coding, but it’s not required for this role.
* You’ve supported at least 50 Salesforce users at some point in your sales operations career. Bonus points if you’ve scaled to 200+ users.
* You’ve used (or at least are aware of) Marketo, Zuora, InsightSquared, DiscoverOrg, Zendesk, and LeanData.
* You share our [values](/handbook/#values), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process, and may be asked to interview additional team members. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Selected candidates will be invited to schedule a screening call with our Global Recruiters
- Next, candidates will be invited to schedule an interview with the Director of Sales Operations
- The strongest candidates may be asked to schedule additional 45 minute interviews with our CRO
- Finally, candidates may interview with our CEO.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
