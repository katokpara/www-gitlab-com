---
layout: job_page
title: "Strategic Account Leader"
---

## Responsibilities

* Strategic Account Leader will report to a Regional Director.
* Act as a primary point of contact and the face of GitLab for our [strategic and large](https://about.gitlab.com/handbook/sales/#market-segmentation) prospects.
* Contribute to post-mortem analysis on wins/losses.
   * Communicate lessons learned to the team, including account managers, the marketing team, and the technical team.
* Take ownership of your book of business
    * document the buying criteria
    * document the buying process
    * document next steps and owners
    * ensure pipeline accuracy based on evidence and not hope
* Contribute to documenting improvements in our [sales handbook](https://about.gitlab.com/handbook/sales/).
* Provide account leadership and direction in the pre- and post-sales process
* Conduct sales activities including prospecting and developing opportunities in large/strategic accounts
* Ensure the successful rollout and adoption of GitLab products through strong account management activities and coordination with pre-and-post sales engineering and support resources
* Be the voice of the customer by contributing product ideas to our public [issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues)
* Travel as necessary to accounts in order to develop relationships and close large opportunities
* Generate qualified leads and develop new customers in conjunction with our strategic channel partners in exceeding quota.
* Expand knowledge of industry as well as the competitive posture of the company
* Prepare activity and forecast reports as requested
* Update and maintain Sales’ database as appropriate
* Assist sales management in conveying customer needs to product managers, and technical support staff
* Utilize a consultative approach, discuss business issues with prospect and develop a formal quote, a written sales proposal or a formal sales presentation addressing their business needs.
* Respond to RFP's and follow up with prospects.
* Develop an account plan to sell to customers based on their business needs.
* Build and strengthen the business relationship with current accounts and new prospects.
* Recommend marketing strategies.

## Requirements

* A true desire to see customers benefit from the investment they make with you
* Able to provide high degree of major account management and control
* Work under minimal supervision on complex projects
* 5+ years of experience with B2B software sales
* Experience selling into large organizations
* Interest in GitLab, and open source software
* Ability to leverage established relationships and proven sales techniques for success
* Effective communicator (written/verbal), strong interpersonal skills
* Motivated, driven and results oriented
* Excellent negotiation, presentation and closing skills
* Preferred experience with Git, Software Development Tools, Application Lifecycle Management
* You share our [values](/handbook/values), and work in accordance with those values.

## Specialties

### Federal

#### Additional Requirements

* 5+ years successful quota attainment in the Federal Space
* DOD Only
  - 5+ years of experience selling into Federal DOD accounts with a comprehensive understanding of the local military landscape and associated business processes.
* Intel Only
  - Must have security clearance
  - 5+ years of experience selling into Federal Intel accounts with a comprehensive understanding of the intelligence community landscape and associated business processes.
* Civilian Only
  - 5+ years of experience selling into Federal Civilian accounts with a comprehensive understanding of the local public sector landscape and associated business processes.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
  1. What would differentiate you and make you a great account executive for GitLab?
  1. What is your knowledge of the space that GitLab is in? (i.e. Industry Trends)
  1. How do you see the developer tools changing over the coming years from a sales perspective? (i.e. Competitive Positioning, Customer Needs, etc)
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with Regional Sales Director (in their same region)
* Candidates will then be invited to schedule a second interview with Regional Sales Director (in a different region)
* Candidates will be invited to schedule a third interview with our CRO
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

## Compensation

You will typically get 50% as base and 50% based on commission. See our [market segmentation](https://about.gitlab.com/handbook/sales/#market-segmentation) for typical quotas in the U.S. Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
