---
layout: job_page
title: "Director of Sales Operations"
---

Your mission is to organize data and generate deep customer insight in order to enhance sales force productivity and effectiveness. You will accomplish your mission by working collaboratively with our sales, marketing, finance and operations teams, you will measure everything to help us optimize our sales funnel and achieve growth. Your impact will help us make it easier to maintain a competitive edge, enhance sales force effectiveness, and achieve consistent, sustainable sales success.

## Responsibilities

* Implementing tools and processes for the sales organization that focus on improving efficiency and effectiveness
* Manage Platforms and Systems: Responsible for all SalesForce.com administration including user management, data management, application setup, customization, reports and dashboards. Zuora, WebEx, TrainTool, etc.
* Partner in the development of sales coverage, headcount planning and sales quota.
* Create and manage New Hire Boot Camps to quickly ramp up Sales Reps
* Improve and manage the [Sales Onboarding Bootcamp](https://about.gitlab.com/handbook/sales-onboarding/)
* Create Board level presentations for the Chief Revenue Officer
* Manage, analyze and summarize the weekly Sales Forecast
* Create and manage programs for Weekly and Quarterly Sales Training sessions
* Organize and manage the sales resource library to include marketing pieces, presentations, outreach campaigns and competitive market intelligence.
* Pricing and Contract Support: Given the pace of business, its imperative that sales operations enable the sales team with high-quality proposals that can be turned around quickly and efficiently.
* Compensation Management
* Manage approval process (deal sign off) between sales and finance

## Requirements

* Minimum 5 years Sales Ops experience required
* Strong analytical ability and able to prioritize multiple projects
* Strong communication skills
* Effective multi-tasking and time management required
* Deadline driven
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
