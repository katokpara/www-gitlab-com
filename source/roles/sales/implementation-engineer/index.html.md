---
layout: job_page
title: "Implementation Engineer"
---

Implementation Engineers provide on-site or remote deployment of GitLab technology and solutions as well as training. The Implementation Engineer will act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices. This role helps the customer to faster benefit from the GitLab solution and realize business value. Meanwhile, Implementation Engineers also need to provide feedback to product management and engineering team on the actual field experience through customer engagements and implementations.

To learn more, see the [Implementation Engineer handbook](/handbook/customer-success/implementation-engineering)

## Responsibilities

- Install & configure GitLab solutions in customer environment as per Statement of Work (SOW)
- Provide technical training sessions remotely and/or on-site
- Develop and implement migration plan for customer VCS & data migration
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Develop & maintain custom scripts for integration or policy to align with custom requirements
- Create detailed documentation for implementation, guides and training.
- Support Sales on quoting PS and drafting SOW to respond PS requests
- Travel required - 40%
- Managing creation of new and maintaining of existing training content

## Requirements

- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, ChatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
  - Installation and operation of Linux operating systems and hardware investigation/manipulation commands
  - BASH/Shell scripting including systems and init.d startup scripts
  - Package management (RPM, etc. to add/remove/update packages)
  - Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)
- Experience with Ruby on Rails applications and Git

## Hiring Process

- Qualified applicants receive a short questionnaire
- Selected candidates will be invited to schedule a 30 minute screening call
- Next, candidates will be invited to schedule technical interviews with a Senior Support Engineer
- Candidates will be invited to schedule interviews with Director of Customer Success
- Next, candidates will be invited to schedule an interview with Regional Sales Director, Solutions Architect
- Candidates may be invited to schedule an interview with our CRO
- Finally, candidates may interview with our CEO
- Successful candidates will subsequently be made an offer via email

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
