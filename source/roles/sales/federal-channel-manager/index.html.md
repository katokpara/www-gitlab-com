---
layout: job_page
title: "Federal Channel Manager"
---

As a Federal Channel Manager your primary goal will be to build pipeline and channel revenue with high performance partners. This high-energy position requires the candidate to be extremely motivated and results-driven -- with follow through to create and qualify partner-initiated opportunities.

## Responsibilities

- Identify and recruit new federal reseller partners in key markets through market analysis and cross-leveraging channel ecosystems of key alliance partners
- Coordinate and conduct sales training for partner onboarding
- Manage sales opportunities with all designated Partners, providing accurate and updated sales funnel.
- Build and maintain quarterly business plans for top partner accounts.
- Development of territory plan, including documenting recruiting strategy.
- Assist Partners in broadening their product expertise and sales opportunities, striving for growth from all associated selling resources.
- Provide timely and on-going deal support to help close new opportunities at identified resellers including pricing requests, resource coordination, and other partner management activities.
- Drive marketing initiatives, go-to-market programs, and demand generation activities that will result in new reseller sales opportunities
- Proactively develop incentives or contests that will increase GitLab’s consideration rate within the reseller’s sales force.
- Provide overall relationship management and act as the central point of contact for channel resellers and distributors.
- Be a key contributor to help define and scale the optimal channel partner program.
- Assist sales management in conveying customer needs to product managers, and technical support staff

## Requirements

- Recent experience in software Federal Channel Sales is mandatory including knowledge of 2-tier (distribution) reseller models.
- Work under minimal supervision on complex projects.
- Strong interpersonal skills and ability to excel in a team oriented atmosphere
- Strong written/verbal communications skills
- Very motivated and goal-oriented
- Must want a career-oriented environment that is both fun and professional.
- Strong customer service orientation and ability to develop and maintain relationships
- You share our values, and work in accordance with those values.

## Hiring Process

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
