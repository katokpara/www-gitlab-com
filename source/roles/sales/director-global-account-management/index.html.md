---
layout: job_page
title: "Director of Global Account Management"
---

As the Director of Global Account Management, you will lead our dynamic account management team and help drive various customer growth initiatives, projects and strategies.

The right candidate will play a critical role in the delivery of a world-class service experience to GitLab's customers and channel partners.

The right candidate is responsible for architecting the post-sales customer experience and lifecycle by building a world class account management organization, implementing technology and processes, and partnering with the sales, product, engineering and operations teams to deliver the best possible customer experience.

The right candidate is truly passionate about customer advocacy and has a proven track record to talk about! This is an exciting opportunity to unequivocally influence our customers and furthermore, directly impact GitLab’s overall success and growth.

## Responsibilities

* Set the overall vision and strategic plan for the Global Account Management organization - with specific focus on leveraging Health, Usage and Adoption metrics to drive engagement and growth with customers. Work closely with the [Director of Customer Success](https://about.gitlab.com/roles/sales/director-customer-success) on these metrics.
* Drive Account Growth Outcomes: Expand our revenue in accounts through up-sell opportunities with our GitLab EE Products, influence future lifetime value through higher product adoption, customer satisfaction and overall health scores, reduce churn and drive new business growth through greater advocacy and reference ability
* Build programs to drive usage and adoption metrics within account base.
* Define and Optimize Customer Lifecycle: define segmentation of customer base and varying strategies, identify opportunities for continuous improvement.
* Measure effectiveness of team: define operational metrics for team, establish system for tracking metrics, create cadence for review within the team
* Lead World-Class Account Management Team: recruit experienced leaders for each functional role, attract high potential individual contributors into the team, create rapid onboarding process for new team members, and foster collaboration within team and across customers
* Deliver transformational leadership so that team is highly motivated and engaged.  Be an inspirational role model by challenging and maximizing the strength of the team and aligning their efforts to the mission and vision of the organization
* Enhance Effectiveness and Efficiency through technology
* Represent the organization at the highest levels to prospective partners and customers. Empower the organization through effective communication
* Align with Sales across cross-sell and up-sell and focus on selling with a retention focus
* Align with Finance around measurement and forecasting with driving profitable gross margin focus

## Requirements

* 7 - 10 years of management experience – including at least three years heading a successful customer success or account management organization in an enterprise software environment
* Strong understanding and knowledge of the account management role in successful [Application Lifecyle Management](https://en.wikipedia.org/wiki/Application_lifecycle_management)) enterprise environments
* Experience with account relationship and growth management of large strategic clients. Must be able to oversee the management of a high volume of accounts at all levels: SMB; Mid Market; Enterprise and Partners
* Experience successfully working with senior (C-level) executives
* Demonstrated ability to lead managers and successfully manage global, distributed teams across cultures, lines of business, and geographies
* Possesses a strong management presence and leadership ability, with communication and interpersonal skills that inspire and motivate leaders and teams
* Demonstrated excellence in analytical thinking, problem solving, communication, delegation, planning & organization and judgment
* Able to be flexible and agile in responding to evolving business priorities and dealing with ambiguity
* Able to collaborate across organization and with external stakeholders
* Holds strong operational skills that will drive organizational efficiencies and customer satisfaction
* Willing and able to address escalated client issues with speed and urgency
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire and coding exercise from our Global Recruiters
* The review process for this role can take a little longer than usual but if in doubt, check in with the Global recruiter at any point.
* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CRO
* Candidates will then be invited to schedule a series of interviews with our Director of Demand Generation and our three Regional Directors of Sales
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
