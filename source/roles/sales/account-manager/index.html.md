---
layout: job_page
title: "Account Manager"
---

## Responsibilities

* Build and expand relationships and awareness of GitLab within SMB accounts
* Identify and document business goals, technology stack, and how customers are using GitLab
* Increase adoption of GitLab features and products through implementation of an outreach strategy
* Advocate for customer needs for training and services where needed
* Manage the renewal process and exceed renewal goals
* Advocate for the overall health of relationship to GitLab acting proactively to address issues and concerns
* Engage with GitLab Support team and GitLab billing team to ensure customer questions are addressed in a timely manner

## Requirements

* 2+ years of successful account management experience managing thousands of SMB customers
* Strong customer orientation, dedication, and passion for delivering a great customer experience
* Excellent verbal and written communication skills
* Collaborative and team oriented
* Experience using Salesforce.com
* Motivated, self-directed and results oriented
* Knowledge of Git and GitLab workflow preferred
* Knowledge of Zuora preferred
* Interest in GitLab, and open source software
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Dir. Customer Success
* Candidates will then be invited to schedule an interview with our CRO
* Finally, candidates may be scheduled for a 50min interview with our CEO
* Successful candidates will subsequently be made an offer via email

Expect between 2-3 business between each step. Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

## Compensation

You will get 70% as base and 30% based on commission. See our [market segmentation](https://about.gitlab.com/handbook/sales/#market-segmentation) for typical quotas in the U.S. Account Managers focus on Mid-Market. Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
