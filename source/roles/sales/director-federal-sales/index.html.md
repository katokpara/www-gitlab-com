---
layout: job_page
title: "Director Federal Sales"
---

The Director of Federal Sales is responsible for building, managing and leading the Federal sales teams by overseeing sales, operations and strategies for new and existing customers of GitLab.

The right candidate will play a critical role in the creation and execution of a Federal Sales Program (Team, Strategy, Systems & Process) to maximize the adoption of GitLab within the Federal market.

## Responsibilities

* Drives, manages and executes the business and revenue of the federal sales team; develops, coaches and trains sales staff
* Analyzes regional market dynamics in an effort to maximize existing successes and to create new sales growth opportunities
* Educates team on significant industry factors including competitive products, regulations, trends, customer needs, and pricing
* Prepares forecasts, territory/industry management, and growth plans
* Establishes and reports on metrics to measure team performance; correct deficiencies where necessary
* Supports quotation and proposal efforts to partners, prospects and customers
* Ensures that the federal sales plan is aligned with and supports the corporate revenue goal
* Recruits, hires and trains staff members; fosters a successful and positive team environment

## Requirements

* At least five 5 years of sales management, selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
* At least 10 years of experience selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
* Must be a high performing sales manager with a proven track record of consistently exceeding established measurements for goals and objectives into Fed
* Must be a skilled motivator with demonstrated ability to coach and develop sales staff to exceed targeted sales objectives and deliver high quality service to customers
* Must have a proven ability to manage both direct and indirect employees in a matrix organization
* Preferred candidates will have experience in business-to-business selling and managing using a consultative sales approach, and will have strong demonstrated partnering and closing skills
* Must have expertise around GSA, Federal procurement and the various buying vehicles to enable growth in software sales via our distribution partners
* Excellent management and communication skills (written and verbal)
* Demonstrated knowledge of different sales methodologies and customer relationship management (CRM) systems: SFDC knowledge is desired
* Knowledge of open source enterprise software and/or version control is highly desirable
* Experience with partner recruitment, enablement and on going support
* Exceptional knowledge of Federal infrastructure and agencies
* Have history of working with and driving revenue within NSF, DOE Labs, DoD and Intel
* A security clearance is a plus

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CRO
* Candidates will then be invited to schedule a second interview with our CRO
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
